<?php

namespace App\Http\Controllers;

use App\Http\Models\Url;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class UrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $model = ['url' => Url::where('user_id', Auth::user()->id)->get()];
        return view('manage/url/index', $model);
    }


    public function data(DataTables $datatables, Request $request)
    {
        $query = Url::with('tag')
            ->where('user_id', Auth::user()->id)
            ->selectRaw('distinct url.*');

        $datatables = $datatables->eloquent($query)
            ->addColumn('action', function (Model $model) {
                $id = $model->id;
                return view('manage.url.button_action', compact('model', 'id'));
            })
            ->addColumn('tag', function (Url $u) {
                return $u->tag->map(function ($t) {
                    return $t->tag;
                })->implode("\n"); //TODO ini nge render nya masih plain text
            });

        return $datatables->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Http\Models\Url $url
     * @return \Illuminate\Http\Response
     */
    public function show(Url $url)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Http\Models\Url $url
     * @return \Illuminate\Http\Response
     */
    public function edit(Url $url)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Http\Models\Url $url
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Url $url)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Http\Models\Url $url
     * @return \Illuminate\Http\Response
     */
    public function destroy(Url $url)
    {
        //
    }
}
