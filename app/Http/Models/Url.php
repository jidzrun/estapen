<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Url extends BaseModel
{
    protected $table = 'url';
    public function tag()
    {
        return $this->belongsToMany(Tag::class);
    }
}
