<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class TagUrl extends BaseModel
{
    protected $table = 'tag_url';
}
