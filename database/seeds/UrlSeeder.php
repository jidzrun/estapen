<?php

use Illuminate\Database\Seeder;

class UrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('id_id');

        for ($i = 0; $i < 3; $i++) {
            \App\Http\Models\Url::create([
                'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'user_id' => \App\User::all()->random()->id,
                'title' => $faker->company,
                'description' => $faker->words(6, true),
                'url' => $faker->url,
                'url_short' => $faker->word
            ]);
        }
    }
}
