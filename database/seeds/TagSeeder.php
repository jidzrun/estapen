<?php

use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('id_ID');
        for ($i = 0; $i < 10; $i++) {
            \App\Http\Models\Tag::create(
                ['id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                    'user_id' => \App\User::all()->random()->id,
                    'tag' => $faker->word,
                    'description' => $faker->sentence(5, true)]
            );
        }
    }
}
