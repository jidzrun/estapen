<?php

use Illuminate\Database\Seeder;

class TagUrlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('id_ID');
        for ($i = 0; $i < 10; $i++) {
            \App\Http\Models\TagUrl::create([
                'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
                'url_id' => \App\Http\Models\Url::all()->random()->id,
                'tag_id' => \App\Http\Models\Tag::all()->random()->id
            ]);
        }
    }
}
