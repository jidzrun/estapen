@extends('layouts\app')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-lg-9">

            </div>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Url</th>
                    <th>Tag</th>
                </tr>
                </thead>
                <tbody>
                @php $i=1 @endphp
                @foreach($url as $u)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{ $u->url }}</td>
                        <td>
                            <ul>
                                @foreach($u->tag as $t)
                                    <li class="badge badge-pill badge-primary">{{ $t->tag }}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    @php $i++ @endphp
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
