@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Tautan Anda</div>

                    <div class="card-body">
                        @if (session('message'))
                            <div class="alert alert-success" role="alert">
                                {{ session('message') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-2">
                                <a href="{{route('url.create')}}" class="button btn btn-primary">Create</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-vcenter js-dataTable-full" id="roles-table">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Tags</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js_after')
    <script>
        $(function () {
            $('#roles-table').DataTable({
                serverSide: true,
                processing: true,
                searchDelay: 1000,
                ajax: '{{ route('url.data') }}',
                columns: [
                    {data: 'title', name: 'url.title'},
                    {data: 'tag', name: 'tag.tag', type: 'html'},
                    {data: 'description', name:'url.description'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
